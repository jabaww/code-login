## Form Login dengan Jsonplaceholder menggunakan library React.js##

Tujuan dari aplikasi yang saya buat adalah untuk menampilkan data yang berada di jsonplaceholder kedalam sebuah aplikasi login form.

## Dependencies Tambahaan ##
-axios
-bulma

## Fungsi yang di terapkan ##

useEffect()
useState()
useNavigate()
Map()
Router
pagination

## Penjelasan singkat aplikasi ##
-pertama masuk menu login masukan data berdasarkan username yang ada total 10 username
-jika username tidak sesuai akan menampilkan pesan data tidak ditemukan
-jika berhasil login akan masuk ke dalam page dashboard
-isi dari dashboard tampilan nama user login, dan data post yang berjumblah 100
-data yang tampil per 10 data dengan pagination



Terimakasih, Happy Coding
